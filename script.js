const replace = require('replace-in-file');
const fs = require('fs');
const csv = require('csv-parser');
const readline = require('readline');

const terminal = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

/**
 * Sort array data by length desc order
 * @param  {array} listData list string data
 * @return {array} list string data after sort by length
 */
function sortByLength(listData) {
    for (let i = 0; i < listData.length - 1; i++) {
        for (let j = 1; j < listData.length; j++) {
            if (listData[i].length < listData[j].length) {
                const temp = listData[j];
                listData[j] = listData[i];
                listData[i] = temp;
            }
        }
    }
    return listData;
}

/**
 * Convert list string data into regex
 * @param {array} listData list string data
 * @param {string} regexFormat regex format that will convert to
 * @return {array} list string data after change regex
 */
function convertToRegexAll(listData, regexFormat) {
    const regexList = [];
    for (let i = 0; i < listData.length; i++) {
        regexList.push(new RegExp(listData[i], regexFormat));
    }
    return regexList;
}

/**
 * Get collapse data with specific column List
 * @param {array} listData list data csv
 * @param {array} columnList list choosen column name
 * @return {array} list data csv after collapsed
 */
function getCollapseData(listData, columnList) {
    const dataList = [];
    for (let index = 0; index < listData.length; index++) {
        const data = listData[index];
        const convertData = {};
        for (let columnIndex = 0; columnIndex < columnList.length; columnIndex++) {
            const key = columnList[columnIndex];
            convertData[key] = data[key];
        }
        dataList.push(convertData);
    }
    return dataList;
}

/**
 * Compare 2 object depend on number propertes of sourceData
 * @param {object} sourceData source object data
 * @param {object} destinationData  destination object data
 * @param {string} columnKey column key
 * @return {boolean} true: equal, false: not equal
 */
function isEqualObjectByKey(sourceData, destinationData, columnKey) {
    if ({}.hasOwnProperty.call(sourceData, columnKey)) {
        return (sourceData[columnKey] === destinationData[columnKey]);
    }
    return false;
}

/**
 * Get unique data
 * @param {array} listData list data
 * @param {string} uniqueColumnKey unique column for comparing
 * @return {array} list unique data
 */
function getUniqueData(listData, uniqueColumnKey) {
    const dataList = [];
    for (let index = 0; index < listData.length; index++) {
        const data = listData[index];
        let isExist = false;
        // terminal.write(`Start check data ${data.toString()}\n`);
        for (let dataIndex = 0; dataIndex < dataList.length; dataIndex++) {
            const dataTemp = dataList[dataIndex];
            // terminal.write(`--Compare with ${dataTemp.toString()}`);
            if (isEqualObjectByKey(data, dataTemp, uniqueColumnKey)) {
                // terminal.write('----Match data----\n');
                dataTemp.count++;
                isExist = true;
                break;
            }
        }
        if (isExist === false) {
            data.count = 1;
            dataList.push(data);
        }
    }
    return dataList;
}
/**
 * Replace text in file
 * @param {array} fromList list string that should be replace
 * @param {array} toList list string that after replace
 * @param {string} filePath file pathe
 * @return {Promise} true: replace successful, false: replace failure
 */
function replaceTextInFile(fromList, toList, filePath) {
    const options = {
        files: filePath,
        from: fromList,
        to: toList,
        dry: true,
        countMatches: true,
    };

    return new Promise((resolve) => {
        (async function() {
            try {
                const results = await replace(options);
                for (let index = 0; index < results.length; index++) {
                    const resultItem = results[index];
                    terminal.write(`Replacement results: ${resultItem}\n`);
                }
                resolve(true);
                console.log(results);
            }
            catch (error) {
                terminal.write(`Error occurred: ${error}\n`);
                resolve(false);
            }
        })();
    });
}

/**
 * Get list header columns from csv row object
 * @param {object} csvRow csv data row object
 * @return {array} list header columns
 */
function getListColumnHeader(csvRow) {
    const propertyList = [];
    for (const property in csvRow) {
        if ({}.hasOwnProperty.call(csvRow, property)) {
            propertyList.push(property);
        }
    }
    return propertyList;
}

/**
 * Convert answer input from user to object
 * @param  {string} answer answer input from user
 * @param  {array} listColumns list column name
 * @return {object} null: incorrect answer, object { index, key }: correct answer
 */
function convertAnswer(answer, listColumns) {
    const number = parseInt(answer, 0);
    if (isNaN(number)) {
        terminal.write('Your input is not a number. Please try again!\n');
        return null;
    }
    else if (number < 0 || number >= listColumns.length) {
        terminal.write(`Your selection is out of list. Please input number from 0 ~ ${listColumns.length - 1}\n`);
        return null;
    }

    terminal.write(`${number}\n`);
    terminal.write(`Your answer: ${listColumns[number]}\n`);
    return {
        index: number,
        key: listColumns[number],
    };
}

/**
 * ask question to choose column source
 * @param {array} listColumns list column name
 * @return {Promise} await data of answer object
 */
function askChooseColumnSource(listColumns) {
    let question = 'There are list column headers in csv file\n';
    for (let index = 0; index < listColumns.length; index++) {
        question += `${index}. ${listColumns[index]}\n`;
    }
    question += 'Please input column number that you want to set as source text\n';
    return new Promise((resolve) => {
        terminal.question(question, (answer) => {
            const answerObject = convertAnswer(answer, listColumns);
            resolve(answerObject);
        });
    });
}

/**
 * ask question to choose column source
 * @param {object} columnSource column source data { index, key }
 * @param {array} listColumns list column name
 * @return {Promise} await data of answer object
 */
function askChooseColumnDestination(columnSource, listColumns) {
    let question = 'There are list column headers in csv file\n';
    for (let index = 0; index < listColumns.length; index++) {
        if (index === columnSource.index) continue;
        question += `${index}. ${listColumns[index]}\n`;
    }
    question += 'Please input column number that you want to set as destination text\n';
    return new Promise((resolve) => {
        terminal.question(question, (answer) => {
            const answerObject = convertAnswer(answer, listColumns);
            resolve(answerObject);
        });
    });
}

/**
 * Get csv data
 * @return {Promise} csv data
 */
function getCsvData() {
    const csvData = [];
    return new Promise((resolve) => {
        fs.createReadStream('data/data.csv')
            .pipe(csv())
            .on('data', (data) => csvData.push(data))
            .on('end', () => resolve(csvData));
    });
}
/**
 * Duplicate data List with prefix and suffix on new list
 * @param {array} dataList data list
 * @param {string} prefix prefix string will insert into all data item
 * @param {string} suffix suffix string will insert into all data item
 * @param {string} columnKey column key should change data
 * @return {array} return new data list
 */
function duplicateDataWithString(dataList, prefix, suffix, columnKey) {
    const newData = dataList.slice(0);
    for (let index = 0; index < newData.length; index++) {
        newData[index][columnKey] = prefix + newData[index][columnKey] + suffix;
    }
    // console.log(dataList);
    return newData;
}
/**
 * convert to regex data preparing to comparing and replace
 * @param {array} dataList data list
 * @param {string} fromColumnKey from column key
 * @param {string} toColumnKey to column key
 * @return {object} { from: array, to: array }
 */
function getReplaceOptions(dataList, fromColumnKey, toColumnKey) {
    // convertToRegexAll
    let newDataList = dataList;
    newDataList = sortByLength(newDataList);

    let fromData = newDataList.map(data => data[fromColumnKey]);
    fromData = convertToRegexAll(fromData);
    const toData = newDataList.map(data => data[toColumnKey]);

    return {
        from: fromData,
        to: toData,
    };
}

/**
 * It's main function, all code will run in here
 */
async function main() {
    let csvData = await getCsvData();

    if (csvData.length === 0) {
        terminal.write('You csv file don\'t have any data. Please change your file and try again!\n');
        return;
    }
    terminal.write(`Total row in csv file: ${csvData.length}\n`);

    const listColumnHeader = getListColumnHeader(csvData[0]);

    let columnSourceAnswer = null;
    while (columnSourceAnswer === null) {
        columnSourceAnswer = await askChooseColumnSource(listColumnHeader);
    }

    let columnDestinationAnswer = null;
    while (columnDestinationAnswer === null) {
        columnDestinationAnswer = await askChooseColumnDestination(columnSourceAnswer, listColumnHeader);
    }

    terminal.write(`Column source: ${columnSourceAnswer.key}\n`);
    terminal.write(`Column destination: ${columnDestinationAnswer.key}\n`);

    const selectedColumn = [
        columnSourceAnswer.key,
        columnDestinationAnswer.key,
    ];
    
    terminal.write('-------------- SUMMARY -----------------\n');
    terminal.write(`Number of row in origin csv: ${csvData.length}, total column: ${listColumnHeader.length}\n`);
    csvData = getCollapseData(csvData, selectedColumn);
    terminal.write(`Number of row after collapse csv: ${csvData.length}, total column: ${selectedColumn.length}\n`);
    csvData = getUniqueData(csvData, columnSourceAnswer.key);
    terminal.write(`Number of row after remove duplicated csv: ${csvData.length}, total column: ${selectedColumn.length}\n`);

    const replaceOption = getReplaceOptions(csvData, columnSourceAnswer.key, columnDestinationAnswer.key);
    terminal.write(`Number of from data: ${replaceOption.from.length}\n`);
    terminal.write(`Number of to data: ${replaceOption.to.length}\n`);

    let result = await replaceTextInFile(replaceOption.from, replaceOption.to, '../../kintone/kntn-eigyo-master/js/*.js');
    terminal.write(result + ' \n');
    result = await replaceTextInFile(replaceOption.from, replaceOption.to, '../../kintone/kntn-houjin-master/js/*.js');
    terminal.write(result + ' \n');
    terminal.close();
}
/**
 * Test data list in console
 * @param {array} dataList data list
 * @param {string} columnKey column key
 */
function testConsoleListData(dataList, columnKey = null) {
    for (let index = 0; index < dataList.length; index++) {
        let data = null;
        if (columnKey === null) {
            data = dataList[index];
        }
        else {
            data = dataList[index][columnKey];
        }
        terminal.write(`Data ${index}: ${data}\n`);
    }
}

main();